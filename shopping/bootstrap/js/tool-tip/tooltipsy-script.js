$(document).ready(function(){
	
	$('.hastip').tooltipsy({
		offset: [0, 10],
		css: {
			'padding': '10px',
			'max-width': '200px',
			'color': '#ffffff',
			'font-size':'13px',
			'background-color': '#000000',
			'border': '1px solid #000000',
			'position' : 'relative',
			'-moz-box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
			'-webkit-box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
			'box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
			'text-shadow': 'none'
		}
 	});
	
						   
});