
$(document).ready(function() {
    $(window).scroll(function(){
        if ($(this).scrollTop() > 200) {
            $('.s-scrollToTop').fadeIn();
        } else {
            $('.s-scrollToTop').fadeOut();
        }
    });


    //Click event to scroll to top
    $('.s-scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });

});