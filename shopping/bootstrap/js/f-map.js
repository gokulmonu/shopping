function initialize() {
	
	
    var loc, map, marker, infobox;
    
    loc = new google.maps.LatLng(10.5016521,76.9434061);
    
    map = new google.maps.Map(document.getElementById("map"), {
         zoom: 13,
		scrollwheel: false,
		navigationControl: false,
		mapTypeControl: false,
		scaleControl: false,
		panControl : false,
		draggable: true,
		scaleControl: false,
		overviewMapControl:false,
		zoomControl:false,
		zoomControlOptions:false,
         center: loc,
         mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    
    marker = new google.maps.Marker({
        map: map,
        position: loc,
        visible: true
    });
	
	
    infobox = new InfoBox({
         content: document.getElementById("infobox"),
         disableAutoPan: false,
         pixelOffset: new google.maps.Size(-140, 0),
         zIndex: null,
         boxStyle: {
            opacity: 1
        },
        closeBoxMargin: "12px 4px 2px 2px",
        closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
        infoBoxClearance: new google.maps.Size(1, 1)
    });
    
    google.maps.event.addListener(marker, 'click', function() {
        infobox.open(map, this);
        map.panTo(loc);
    });
	
	
	
	map.set('styles', [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#afafaf"},{"visibility":"on"}]}]);

	
};
google.maps.event.addDomListener(window, 'load', initialize);