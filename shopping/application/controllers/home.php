<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {


        function __construct() {
            parent::__construct();
        }


         function index(){
             $this->load->library('../controllers/product');
             $this->product->listing();

	     }

        public function contactUs()
        {
            $this->load->template('contact_us');
        }
        public function aboutUs()
        {
            $this->load->template('about_us');

        }
        public function services()
        {
            $this->load->template('services');
        }
        public function careers()
        {
            $this->load->model('career_model');
            $careerList = $this->career_model->getListByQuery('select * from sb_career where is_deleted is null');
            $this->load->template('carriers',$careerList);

        }
        public function stores()
        {
            $this->load->template('our_stores');

        }
    public function sendEmail()
    {
        $to      = 'info@nelkecompany.com';
        $subject = 'Enquiry Mail';
        $name = $this->input->post('name');
        $fromEmail = $this->input->post('email');
        $message = $this->input->post('message');
        $phone = $this->input->post('phone');
        $mailBody = '<html><body>';
        $mailBody .='<div>
                             <h2>Contact Form Submission</h2>
                               <div style="width: 300px;height:200px;">
                                <p><span>From</span></p>';
        $mailBody .= $name." "."</br>";
        $mailBody .= $fromEmail."</br>";
        $mailBody .= $phone."</br>";

        $mailBody .= '</div>';
        $mailBody .='<div><h5>Dear Admin,</h5><h6>&nbsp;&nbsp;&nbsp;&nbsp;
                                Please accept this letter as formal application for using your service.<br>
                                </h6>';

        $mailBody .= '</div>';
        $mailBody .= '</div>';
        $mailBody .='<table width="100%" border="1">';
        $mailBody.= $message;
        $mailBody .='</table>';
        $mailBody .='</body></html>';

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';

        $headers[] = 'To: Admin <'.$to.'>';
        $headers[] = 'From: '.$name.' <'.$fromEmail.'>';
        $params = '-f ' . $fromEmail;

        $status = mail($to, $subject, $mailBody, implode("\r\n", $headers),$params);
        $result = ['success' =>$status];
        header('Content-Type: application/json');
        echo json_encode( $result );
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */