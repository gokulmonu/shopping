<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
        $this->load->model('product_model','product_model');

    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "login"){
           $this->login();
        }elseif(trim($method) == "validate"){
            $this->validate();
        }elseif(trim($method) == "logout"){
            $this->logout();
        }
    }

        
       public function login()
       {
           $isLoggedIn = $this->core_lib->getLoginUserId();
           if(!$isLoggedIn){
               $data = [];
               $url = parse_url($_SERVER['REQUEST_URI']);
               if(isset($url['query'])){
                   parse_str($url['query'], $params);
                   if(isset($params['redirect']) && !empty($params['redirect'])){
                       $data['redirect'] = $params['redirect'];
                   }
               }


               $this->load->view('login',$data);
           }else{
               $baseUrl = base_url();
               header('Location:'.$baseUrl);
           }

       }
        public function logout()
        {
            $this->load->library('default_session');
            $this->default_session->destroy();
            redirect('/login', 'refresh');
        }
       public function validate()
        {
            $password = $this->input->post('password');
            $username = $this->input->post('username');
            $decodedString = base64_decode($password);
            $this->form->setRules($username,'username_error','Please enter your username');
            $this->form->setRules($password,'password_error','Please enter your password');
            $validationArr = $this->form->run();
            if(is_array($validationArr) && count($validationArr) && $validationArr != false){
                header('Content-Type: application/json');
                echo json_encode( $validationArr );
            }else{
               // echo password_hash("rasmuslerdorf", PASSWORD_DEFAULT);
                $passwordHash = $this->user_model->getPasswordHash($username);
                if(count($passwordHash)){
                    if(password_verify($decodedString,$passwordHash['password'])){
                        $this->load->library('default_session');
                        $this->default_session->set('user_id',$passwordHash['id']);
                        $redirectUrl['url'] = [
                            'success' => true
                        ];
                        header('Content-Type: application/json');
                        echo json_encode( $redirectUrl );
                    }else{
                        header('Content-Type: application/json');
                        $redirectUrl['url'] = [
                            'success' => false
                        ];
                        echo json_encode( $redirectUrl );
                    }
                }
            }
        }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */