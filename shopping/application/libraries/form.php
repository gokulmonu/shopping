<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form
{

    public $fields = [];

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('user/user_model','user_model');
    }

    public function setRules($fields ,$id='',$label = '',$type ='',$min = 0 ,$max = 0)
    {
        $tempArr = [
            'field' =>$fields,
            'label' => $label,
            'field_id' =>$id,
            'type' =>$type,
            'min' =>$min,
            'max' =>$max
        ];
        $this->fields[] = $tempArr;


    }
    public function run()
    {
        $resultArr = [];
        if(count($this->fields)){
            $flag = true;

            foreach($this->fields as $value){
                $errorMsg = '';
                if(empty($value['field'])){
                    $flag = false;
                    $errorMsg.=$value['label'];
                }
                if($value['field']){
                  if($value['type']){
                    switch($value['type']){
                        case "integer":
                            if(!is_numeric($value['field'])) {
                                $flag = false;
                                $errorMsg.= " "."Please enter numeric values";
                            }

                            break;
                        case "string":
                            if(!is_string($value['field'])){
                                $flag = false;
                                $errorMsg.= " "."Please enter  string values";
                            }

                            break;
                        case "email":
                            if (!filter_var($value['field'], FILTER_VALIDATE_EMAIL)) {
                                $flag = false;
                                $errorMsg.= " "."Please enter  valid email address";
                            }
                        break;
                        default:
                            break;
                    }
                  }
                    if($value['min']){
                        if(strlen($value['field'])< $value['min']){
                            $errorMsg.= " "."Please enter minimum length of ".$value['min'];
                            $flag = false;
                        }
                    }

                    if($value['max']){
                        if(strlen($value['field'])> $value['max']){
                            $errorMsg.= " "."Maximum length allowed is ".$value['max'];
                            $flag = false;
                        }
                    }

                }
                if(!$flag){
                    $tempArr = [
                        'field_id' =>$value['field_id'],
                        'label' => $errorMsg
                    ];
                    $resultArr[] = $tempArr;
                }


            }
        }
        if(count($resultArr)){
            $resultArr['success'] = false;
            return $resultArr;
        }else return false;
    }


}