<?php
$admin = false;
$userGroup= $this->core_lib->getUserGroup();
if(in_array("admin",$userGroup)){
    $admin  = true;
}
if(isset($data)&& isset($data['rows']) && count($data['rows'])){
    foreach ($data['rows'] as $value){
        $date = date('Y-m-d H:i:s',strtotime('yesterday'));
        ?>
        <div class="s-col-lg-3 s-col-md-3 s-col-sm-6 s-col-xs-6"><!--f col-->


            <article class="s-wrp each-product <?php if($value['created'] > $date)echo "spcl-product"; ?>"><!--each-product-->

                <div class="product-img s-wrp"><!--product-img-->
                    <?php
                    $img = !empty($value['default_image'])?$value['default_image'] :$this->config->item('default_image_url');
                    $thumbimg = !empty($value['thumbnail'])?$value['thumbnail']:$this->config->item('default_thumb_image_url');
                    ?>
                    <a href="<?php echo $img?>" class="fancybox-effects-d fancy-large"><img  alt="product1" src="<?php echo $thumbimg?>"></a>

                </div><!--/. product-img-->

                <div class="product-content s-wrp"><!--product-content-->

                    <div class="box"><!--f in-->

                        <h3 class="product-title s-txt-center"><a href="<?php echo base_url().'product/view/'.$value['id']?>"><?php echo $value['name'] ?></a></h3>


                     <!--   <article class="product-descrip s-wrp">
                            <p><?php /*if (strlen($value['description']) > 110) {
                                    echo substr($value['description'], 0, 109) . '..';
                                } else {
                                    echo $value['description'];
                                } */?></p>
                        </article>-->

                    </div><!--/. f in-->

                    <article class="product-meta-grp s-wrp"><!--product-meta-grp-->

        <?php
        $isLoggedIn = $this->core_lib->getLoginUserId();
        if($admin){
           $class = "s-col-lg-6 s-col-md-6 s-col-sm-6 s-col-xs-6";
        }else{
            $class = "s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12";
        }
        if($isLoggedIn){?>

            <ul>
                <li class="<?php echo $class ?>">
                    <a class="prod-btn" href="<?php $string =str_replace(' ','-',$value['name']);  echo base_url().'product/view/'.$string."/".$value['id']?>"><i class="fa fa-eye" aria-hidden="true"></i>VIEW</a></li>


                <?php if($admin){?>
                <li class="<?php echo $class ?>">
                    <a href="<?php echo base_url().'product/edit/'.$value['id']?>" class="prod-btn"  title="Add To Cart"><i class="fa fa-shopping-bag" aria-hidden="true"></i>EDIT</a> </li>

            <?php } ?>


                        </ul>

            <?php

        }else{
            ?>

            <ul>
                <li class="<?php echo $class ?>">
                    <a class="prod-btn" href="<?php $string =str_replace(' ','-',$value['name']);  echo base_url().'product/view/'.$string."/".$value['id']?>"><i class="fa fa-eye" aria-hidden="true"></i>VIEW</a></li>


            </ul>

            <?php
        }
        ?>


                    </article><!--/. product-meta-grp-->


                </div><!--/. product-content-->


            </article><!--/. each-product-->



        </div><!--/. f col-->

    <?php
    }
}else{
    ?>
    <span>No Products Found</span>
    <?php
}

if(isset($data) && isset($data['pagination'])){
    $dataArr = [
        'pagination'=>$data['pagination']
    ];
    $this->view('pagination',$dataArr);
}

?>
