
<div class="s-wrp s-in">

    <ul class="image-gallery s-wrp">

    <?php
if(isset($images) &&  count($images) ){
    foreach($images as $value){
        ?>
       <li <?php if(isset($value['set_default']) && $value['set_default'] == 1){?>class="default" <?php }  ?> >
                    <a><img src="<?php echo $value['thumbnail'] ?>"></a>
                    <h6 class="s-txt-center"><?php if(strlen($value['file_name'])>25)echo substr($value['file_name'],0,25) ?></h6>
                    <ul class="img-btns">
                        <li><a  onclick="setDefaultImage('<?php echo $value['id'] ?>')"><i class="fa fa-check"></i> </a></li>
                        <li><a onclick="removeImage('<?php echo $value['id'] ?>')"><i class="fa fa-close"></i> </a></li>
                    </ul>
                </li>

        <?php
    }
}
?>

    </ul>



</div>

