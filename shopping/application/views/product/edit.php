
<script>
    Dropzone.autoDiscover = false;
    var counter;
    var variantCount;
    var valueCount;
    <?php
    if(isset($specifications)){?>
        counter = parseInt('<?php if(isset($specifications))echo count($specifications)+1 ?>');
    <?php }else{?>
    counter = 1;
    <?php

    }
    ?>
    <?php
    if(isset($variant)){?>
    variantCount = parseInt('<?php if(isset($variant))echo count($variant)+1 ?>');
    <?php }else{?>
    variantCount = 1;
    <?php

    }
    ?>
    <?php
    if(isset($variant)){
    $total = 0;
    foreach($variant as $value){
    $total += count($value);
    }
    ?>
    valueCount = parseInt(<?php echo $total + 1 ?>);
    <?php }else{?>
    valueCount = 1;
    <?php

    }
    ?>
    $(function () {
        getTree();
        $("#category_container").iziModal({
            title: 'Categories',
            subtitle: 'Choose any one the category from below',
            headerColor: '#0caad1',
            width: 600,
            autoOpen: 0,
            fullscreen:true,
            onClosed: function(){
                history.pushState('', document.title, window.location.pathname);

            }
        });
        $("#upload_error_container").iziModal({
            title: 'Upload Error',
            headerColor: '#f4511e',
            width: 600,
            autoOpen: 0,
            timeout : 3000,
            timeoutProgressbar : true,
            fullscreen:false,
            onClosed: function(){
                history.pushState('', document.title, window.location.pathname);

            }
        });
        $("div#drop_zone").dropzone({
            url: '<?php echo base_url()?>product/uploadProductImage',
            success: function (file, response) {
                if(response.error){
                    $('#upload_error').html(response.error);
                    $('#upload_error_container').iziModal('open');
                }else{
                    reloadImages();
                }
                //this.removeFile(file);

            }
        });

        $('#category_but').on('click', function (event) {
            event.preventDefault();
            $('#category_container').iziModal('open');
        });

        $('#add_specification').unbind().on('click', function () {
            var secElem = document.createElement('section');
            secElem.setAttribute('id', 'section_' + counter);
            secElem.setAttribute('name', 'section_' + counter);
            secElem.setAttribute('class', '');

            var button = document.createElement('input');
            button.setAttribute('type','button');
            button.setAttribute('value','x');
            button.setAttribute('onclick','removeSection('+counter +')');

            var label = document.createElement("label");
            var text = document.createTextNode('Type :');
            label.appendChild(text);

            var inputLabel = document.createElement('input');
            inputLabel.setAttribute('type', 'text');
            inputLabel.setAttribute('id', 'input_label_' + counter);
            inputLabel.setAttribute('name', 'input_label_' + counter);
            inputLabel.setAttribute('class', '');

            var label2 = document.createElement("label");
            var text2 = document.createTextNode('Value :');
            label2.appendChild(text2);

            var inputLabel2 = document.createElement('input');
            inputLabel2.setAttribute('type', 'text');
            inputLabel2.setAttribute('id', 'input_label_value_' + counter);
            inputLabel2.setAttribute('name', 'input_label_value_' + counter);
            inputLabel2.setAttribute('class', '');

            secElem.appendChild(button);
            secElem.appendChild(label);
            secElem.appendChild(inputLabel);

            secElem.appendChild(label2);
            secElem.appendChild(inputLabel2);
            $('#specification_container').append(secElem);
            counter = counter + 1;

        });
        $('#add_variant').unbind().on('click', function () {
            var secVariantElem = document.createElement('section');
            secVariantElem.setAttribute('id', 'section_variant_' + variantCount);
            secVariantElem.setAttribute('name', 'section_variant_' + variantCount);
            secVariantElem.setAttribute('class', '');

            var buttonVariant = document.createElement('input');
            buttonVariant.setAttribute('type','button');
            buttonVariant.setAttribute('value','x');
            buttonVariant.setAttribute('onclick','removeSectionVaraint('+variantCount +')');

            var labelVaraint = document.createElement("label");
            var textVariant = document.createTextNode('Variant Type :');
            labelVaraint.appendChild(textVariant);

            var inputLabelVariant = document.createElement('input');
            inputLabelVariant.setAttribute('type', 'text');
            inputLabelVariant.setAttribute('id', 'input_label_variant_' + variantCount);
            inputLabelVariant.setAttribute('name', 'input_label_variant_' + variantCount);
            inputLabelVariant.setAttribute('class', '');

            secVariantElem.appendChild(buttonVariant);
            secVariantElem.appendChild(labelVaraint);
            secVariantElem.appendChild(inputLabelVariant);

            var buttonVariantValue = document.createElement('input');
            buttonVariantValue.setAttribute('type','button');
            buttonVariantValue.setAttribute('value','+');
            buttonVariantValue.setAttribute('onclick','addVaraintValue('+variantCount +')');

            secVariantElem.appendChild(buttonVariantValue);
            $('#product_variant_container').append(secVariantElem);
            variantCount = variantCount + 1;

        });

        $("#product_form").submit(function( event ) {
            event.preventDefault();
            var formData = getFormData();
            $.ajax({
                url: '<?php echo base_url()?>product/validate',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify( formData),
                success: function (data) {
                    $('#product_name_error').html('');
                    if(data.success){
                        window.location.href ='<?php echo base_url() ?>';
                    }

                    if(!data.success){
                        for(key in data){
                            $('#'+data[key]['field_id']).html(data[key]['label']);
                        }
                    }

                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });

        function getFormData() {

            var data = {};
            var tempArr = [];
            var variantArr = [];
            data['id'] = $('#id').val();
            data['name'] = $('#product_name').val();
            data['category_id'] = $('#category_id').val();
            data['description'] = $('#product_description').val();
            $('#specification_container').find('section').each(function () {
                var temp = {};
                var sectionId = $(this).attr('id');
                var id = sectionId.substring(sectionId.lastIndexOf('_')+1);
                temp['type'] = $('#input_label_'+id).val();
                temp['value'] = $('#input_label_value_'+id).val();
                tempArr.push(temp);
            });
            data['specification'] = tempArr;

            $('#product_variant_container').children('section').each(function () {
                var variant = [];
                var tempVariant = {};
                var sectionId = $(this).attr('id');
                var id = sectionId.substring(sectionId.lastIndexOf('_')+1);
                tempVariant['type'] = $('#input_label_variant_'+id).val();

                $('#section_variant_'+id).find('section').each(function () {
                    var subTemp = {};
                    var sectionSubId = $(this).attr('id');
                    var subId = sectionSubId.substring(sectionSubId.lastIndexOf('_')+1);
                    subTemp['value'] = $('#input_label_variant_value_'+id+'_'+subId).val();
                    variant.push(subTemp);
                });
                tempVariant['value'] = variant;
                variantArr.push(tempVariant);
            });
            data['variant'] = variantArr;
            return data;
        }

    });
    function addVaraintValue(variantOptionCount)
    {
        var secVariantValueElem = document.createElement('section');
        secVariantValueElem.setAttribute('id', 'section_variant_value_' + variantOptionCount +"_"+valueCount);
        secVariantValueElem.setAttribute('name', 'section_variant_value_' + variantOptionCount+"_"+valueCount);
        secVariantValueElem.setAttribute('class', '');

        var buttonVariantValue = document.createElement('input');
        buttonVariantValue.setAttribute('type','button');
        buttonVariantValue.setAttribute('value','x');
        buttonVariantValue.setAttribute('onclick','removeSectionVaraintValue('+variantOptionCount +','+valueCount+')');


        var inputLabelVariantValue = document.createElement('input');
        inputLabelVariantValue.setAttribute('type', 'text');
        inputLabelVariantValue.setAttribute('placeholder', 'value');
        inputLabelVariantValue.setAttribute('id', 'input_label_variant_value_' + variantOptionCount+"_"+valueCount);
        inputLabelVariantValue.setAttribute('name', 'input_label_variant_value_' + variantOptionCount+"_"+valueCount);
        inputLabelVariantValue.setAttribute('class', '');

        secVariantValueElem.appendChild(buttonVariantValue);
        secVariantValueElem.appendChild(inputLabelVariantValue);


        $('#section_variant_'+variantOptionCount).append(secVariantValueElem);
        valueCount = valueCount + 1;
    }
    function removeSection(counter) {
        $('#section_' + counter).remove();
    }

    function removeSectionVaraint(counter) {
        $('#section_variant_' + counter).remove();
    }

    function removeSectionVaraintValue(counter , value) {
        $('#section_variant_value_' + counter +'_'+value).remove();
    }

    function removeImage(fileId) {
        var postData = {};
        postData['file_id'] = fileId;
        $.ajax({
            url: '<?php echo base_url()?>product/removeImage',
            type: 'POST',
            data: postData,
            success: function (data) {

                reloadImages();
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }

    function setDefaultImage(fileId) {
        var postData = {};
        postData['file_id'] = fileId;
        postData['product_id'] = $('#id').val();
        $.ajax({
            url: '<?php echo base_url()?>product/set-default-image',
            type: 'POST',
            data: postData,
            success: function (data) {

                reloadImages();
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
    $body = $("body");
    function reloadImages() {
        $body.addClass("loading");
        setTimeout(loadImages,3000);
    }

    function loadImages() {
        var id = $('#id').val();
        $.ajax({
            url: '<?php echo base_url()?>product/getProductImageForRender',
            type: 'POST',
            dataType: "html",
            data:{
                id:id
            },
            success: function (data) {
                $body.removeClass("loading");
                $('#image_container').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
    function getTree() {

        $.ajax({
            url: '<?php echo base_url()?>category/getTree',
            type: 'POST',
            success: function (data) {
                $('#tree').treeview({
                    data: data,
                    expandIcon:'fa fa-plus',
                    collapseIcon:'fa fa-minus',
                    selectedBackColor:'#FA8072',
                    levels:0,
                    onNodeSelected:function (event, data) {
                        $('#category_id').val(data.href);
                        $('#category_html').html(data.text);
                        $('#category_container').iziModal('close');
                    }

                });
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
</script>


<section class="s-wrp s-hi-pad"><!-- section wrp-->
    <div class="s-container"><!-- s-container-->

        <div class="s-wrp"><!--s-wrp-->

            <div class="s-row"><!--s row-->

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                    <article class="s-wrp edit-block">

                        <h2 class="main-title">Add / Edit Product</h2>

                    </article>
                </div>
                <!--/. s col-->

            </div>
            <!--/. s row-->

        </div>
        <!--/. s-wrp-->
    </div>
    <!--/. s-container-->
</section>
<!--/. section wrp-->


<section class="s-wrp"><!-- section wrp-->
    <div class="s-container"><!-- s-container-->

        <div class="s-wrp"><!--s-wrp-->

            <div class="s-row"><!--s row-->

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->


                <form id="product_form" class="custom-form">
                    <input type="hidden" id="id" value="<?php if(isset($id))echo $id; ?>">
                    <div class="s-form-group">
                        <label for="product_name">Product Name:</label>
                        <input type="text" id="product_name" name="product_name" placeholder="Enter Product Name" value="<?php if(isset($name))echo $name; ?>">
                        <span class="error-msg" id="product_name_error"></span>
                    </div>

                    <div class="s-form-group">
                        <label for="product_discount">Category:</label>
                        <input type="button" class="form-btn"  id="category_but" value="select category">
                        <input type="hidden" id="category_id" value="<?php if(isset($category_id))echo $category_id; ?>">
                        <label id="category_html"><?php if(isset($category)){echo $category;}else{echo 'No category selected';} ?></label>
                    </div>
                    <div class="s-form-group">
                        <label for="product_description">Description:</label>
                        <textarea id="product_description"  placeholder="Enter Product Description" name="product_description"><?php if(isset($description))echo $description; ?></textarea>
                        <span class="error-msg" id="product_description_error"></span>
                    </div>

                    <div class="s-form-group">
                        <input type="button" class="form-btn" value="Add specifications" id="add_specification"/>
                    </div>

                    <div class="s-form-group counter-sec" id="specification_container">
                        <?php
                        if(isset($specifications) && count($specifications)){

                            $counter = 1;
                            foreach ($specifications as $value){
                                ?>
                                <section  id="section_<?php echo $counter ?>">
                                    <input type="button"  value="x" onclick="removeSection('<?php echo $counter ?>')">
                                    <label>Type:</label>
                                    <input id="input_label_<?php echo $counter ?>" type="text" value="<?php echo $value['s_type'] ?>" name="input_label_<?php echo $counter ?>">
                                    <label>Value:</label>
                                    <input id="input_label_value_<?php echo $counter ?>" class="" type="text" value="<?php echo $value['s_value'] ?>" name="input_label_value_<?php echo $counter ?>">
                                </section>
                                <?php
                                $counter ++;
                            }
                        }
                        ?>
                    </div>
                    <div class="s-form-group">
                        <input type="button" class="form-btn" value="Add Product Variant" id="add_variant"/>
                    </div>
                    <div id="product_variant_container"  class="s-form-group counter-sec each-variant">
                        <?php
                        $variantCounter = 1;
                        $variantValCounter = 1;
                        if(isset($variant) && count($variant)){
                        foreach ($variant as $key => $value){?>


                        <section  id="section_variant_<?php echo $variantCounter ?>">
                            <input type="button" value="x" onclick="removeSectionVaraint(<?php echo $variantCounter ?>)">
                            <label>Variant Type :</label>
                            <input id="input_label_variant_<?php echo $variantCounter ?>" class="" value="<?php echo $key ?>" type="text" name="input_label_variant_<?php echo $variantCounter ?>"/>
                            <input type="button" value="+" onclick="addVaraintValue(<?php echo $variantCounter ?>)" />
                            <?php
                            if(count($value)){
                                foreach($value as $v){?>
                                    <section id="section_variant_value_<?php echo $variantCounter ?>_<?php echo $variantValCounter ?>" class="">
                                        <input type="button" value="x" onclick="removeSectionVaraintValue(<?php echo $variantCounter ?>,<?php echo $variantValCounter ?>)">
                                        <input id="input_label_variant_value_<?php echo $variantCounter ?>_<?php echo $variantValCounter ?>" class="" type="text" placeholder="value" value="<?php echo $v; ?>">
                                    </section>
                                <?php $variantValCounter++;
                                }
                            }
                            ?>
                        </section>

                        <?php $variantCounter++; }

                        }
                        ?>
                    </div>

                    <label for="drop_zone">Add Files:</label>
                <div id="drop_zone" class="dropzone">

                </div>
                    <div id="upload_error_container">
                        <span id="upload_error"></span>
                    </div>


                    <div id="image_container">
                        <div class="s-wrp s-in">
                            <ul class="image-gallery s-wrp">
                        <?php
                        if(isset($images) && count($images)){

                            $counter = 1;
                            foreach ($images as $value){
                                ?>


                                        <li <?php if(isset($value['set_default']) && $value['set_default'] == 1){?>class="default" <?php }?> >
                                            <a><img src="<?php echo $value['thumbnail'] ?>"></a>
                                            <h6 class="s-txt-center"><?php if(strlen($value['file_name'])>25)echo substr($value['file_name'],0,25) ?></h6>
                                            <ul class="img-btns">
                                                <li><a onclick="setDefaultImage('<?php echo $value['id'] ?>')"><i class="fa fa-check"></i> </a></li>
                                                <li><a onclick="removeImage('<?php echo $value['id'] ?>')"><i class="fa fa-close"></i> </a></li>
                                            </ul>
                                        </li>

                                <?php
                                $counter ++;
                            }
                        }
                        ?>
                            </ul>
                        </div>
                    </div>
                    <span class="error-msg" id="username_error"></span>


                    <div class="s-form-group s-wrp">
                        <input type="submit" class="pg-btn" value="Save">
                    </div>
                </form>


                </div>
                <!--/. s col-->

            </div>
            <!--/. s row-->

        </div>
        <!--/. s-wrp-->
    </div>
    <!--/. s-container-->
</section>
<!--/. section wrp-->


<div id="category_container">
    <div id="tree"><!--tree-->

    </div><!--tree-->
</div>
<div class="modal"></div>