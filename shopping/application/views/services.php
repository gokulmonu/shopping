<section class="s-wrp"><!--second_section-->

    <div class="s-wrp title-strip">
        <h2 class="s-txt-center white-txt">Services</h2>
    </div>

    <div class="s-container"><!--container-->


                <article class="s-wrp s-md-pad">


                    <div class=" s-row"><!--s-row-->
                        <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                            <article class="s-wrp each-service">

                              <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/import.jpg"> </span>

                               <h5 class="s-txt-up">Import</h5>

                                <p>
                                    We import a number of items for wholesale and retail distribution. We also sell imported items at our retail outlets, providing quality products at affordable prices.

                                </p>

                            </article>


                        </div><!--/. s-col-->
 <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                            <article class="s-wrp each-service">

                              <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/wholesale.jpg"> </span>

                               <h5 class="s-txt-up">Wholesale Business</h5>

                                <p>
                                    Nelké Company imports various commodities and offers them for wholesale to retailers. We provide excellent service to our corporate clients by giving them flexibility in the business environment.
                                </p>

                            </article>


                        </div><!--/. s-col-->
 <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                            <article class="s-wrp each-service">

                              <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/retail.jpg"> </span>

                               <h5 class="s-txt-up">Retail Business</h5>

                                <p>
                                    In addition to offering products for wholesale, we also retail various items mostly at our retail outlets. We offer products of the finest selection and the best in quality.


                                </p>

                            </article>


                        </div><!--/. s-col-->
 <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                            <article class="s-wrp each-service">

                              <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/resort.jpg"> </span>

                               <h5 class="s-txt-up">Corporate and Resort supply</h5>

                                <p>
                                    We at Nelke Pvt Ltd take pride with the service we provide to all our customers. We have a dedicated team to cater to our corporate and resort clients who are an important stakeholder for us.


                                </p>

                            </article>


                        </div><!--/. s-col-->
<div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                            <article class="s-wrp each-service">

                              <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/logistics.jpg"> </span>

                               <h5 class="s-txt-up">Logistics</h5>

                                <p>
                                    We have a logistics team that is trained to make sure all our deliveries are made on time. This is a key contributor to the good relationship we have with all our clients.

                                </p>

                            </article>



                    </div><!--/. s-row-->
                </article>




    </div><!--/. container-->
</section><!--/. second_section-->