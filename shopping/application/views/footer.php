<div class="s-pgwrp"><!--s-pgwrp-->
<footer class="s-wrp footer-wrp"><!--footer wrp-->

    <section class="s-wrp s-hi-pad"><!--f wrp-->

        <div class="s-container"><!-- s-container-->

            <div class="s-row"><!--f row-->

                <div class="s-col-lg-4 s-col-md-4 s-col-sm-12 s-col-xs-12"><!--f col-->

                    <h3 class="footer-title s-sec-block">About</h3>


                    <article class="s-wrp"><!--f wrp-->
                        <p class="s-md-pad">Nelké Company Pvt. Ltd was established in 2004. The company strives to provide all its clients with the best products and service. 3 years ago Nelké Company expanded its business to the retail of general hardware and construction materials.... </p>

                        <span class="btn-wrp"> <a class="pg-btn" href="<?php echo base_url()?>about-us">READ MORE</a></span>

                    </article><!--/. f wrp-->


                </div><!--/. f col-->


                <div class="s-col-lg-4 s-col-md-4 s-col-sm-12 s-col-xs-12"><!--f col-->

                    <h3 class="footer-title s-sec-block">My Account</h3>


                    <article class="s-wrp"><!--f wrp-->

                        <ul class="footer-nav">

                            <li><a href="#">My Profile</a></li>
                            <li><a href="<?php echo base_url()?>services">Services</a></li>
                            <li><a href="<?php echo base_url()?>careers">Careers</a></li>
                            <li><a href="<?php echo base_url()?>our-stores">Our Stores</a></li>
                            <li><a href="<?php echo base_url()?>user/add">Signup</a></li>
                        </ul>



                    </article><!--/. f wrp-->


                </div><!--/. f col-->


                <div class="s-col-lg-4 s-col-md-4 s-col-sm-12 s-col-xs-12"><!--f col-->

                    <h3 class="footer-title s-sec-block">Contact Us</h3>


                    <article class="s-wrp address-block"><!--f wrp-->

                        <span>H.Dhilaas 2nd Floor  </span>
                        <span> Janavaree Magu </span>
                           <span> Male 20047 </span>
                           <span> Rep of Maldives</span>




                    </article><!--/. f wrp-->

                    <article class="contact-wrp s-wrp"><!--contact-wrp-->

                        <span class="phone">Tel +960 3004010</span>
                        <span class="fax">Fax +960 3304860</span>

                        <span class="mail">Email:: info@nelkecompany.com


</span>                        </article>
                    <!--/. contact-wrp-->

                    <article class="social-media s-wrp"><!--social-media-->

                        <ul>
                            <li><a target="_blank" href="https://www.facebook.com/hardwarelab.nelke"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
                        </ul>

                    </article><!--/. social-media-->

                </div><!--/. f col-->


            </div><!--/. f row-->

            <a class="s-scrollToTop" style="display: inline;">
                <i class="fa  fa-angle-double-up"></i>
            </a>


        </div><!--/. s-container-->

    </section><!--/. f wrp-->


</footer><!--/. footer wrp-->

<section class="s-wrp copyright-wrp">  <!--f wrp-->

    <div class="s-container"><!-- s-container-->

        <p class="copyright-txt">&copy; 2016 Hardware. All rights resevered. </p>


    </div><!--/. s-container-->

</section>   <!--/. f wrp-->

</div><!--/. s-pgwrp-->