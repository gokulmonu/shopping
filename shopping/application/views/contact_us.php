<script>

    $(function () {
        $("#contact_form").submit(function( event ) {
            event.preventDefault();
            var name = $('#name').val();
            var email = $('#email').val();
            var phone = $('#phone').val();
            var message = $('#message').val();
            $.ajax({
                url: '<?php echo base_url()?>contactus/send-email',
                type: 'POST',
                data: {'name' :name,'email':email,'phone':phone,'message':message},
                success: function (data) {
                    if(data.success){
                        alert("We have received your submission. Our admin will contact you as soon as possible");
                        window.location.href ='<?php echo base_url() ?>';
                    }

                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });
    });

    function initMap() {
        var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxL6dcqSf_0YMSlpXcM5Pa5YGs05xDyOs&callback=initMap">
</script>

<section class="s-wrp s-md-pad "><!--fourth_section-->
    <div class="s-container"><!--container-->
        <article class="s-wrp partner-box contact-box"><!--s-wrp-->



            <!--Vertical Tab-->
            <div id="parentVerticalTab">
                <ul class="resp-tabs-list hor_1">
                    <li>Head Office</li>
                    <li>Hardware Lab (Janavaree Magu)</li>
                    <li>Hardware Lab (Buruzu Magu)</li>
                    <li>Hardware Lab (Baoduthakurufaanu Magu)</li>
                </ul>
                <div class="resp-tabs-container hor_1">
                    <div>
                        <article class="s-wrp adress-block">
                            <div class="address-item s-wrp">
                                <div class=" s-row"><!--s-row-->
                                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12 "><!-- s-col-->

                                     <span class="divide-address s-wrp">
                                         <p>Nelke Company Pvt.  Ltd.</p>
                                        <p> H. Dhilaas 2nd Floor</p>
                                        <p>Janavaree Magu</p>
                                        <p>Male 20047</p>
                                        <p>Republic of Maldives</p>
                                    </span>

                                        <span class="s-wrp s-lt-pad">
                                         <p>Tel:<a class="tel-num">+960 3304861, +960 3004011 </a></p>
                                         <p>Fax:<a class="tel-num">+960 3304860</a></p>
                                         <p>E-mail:<a class="tel-num">info@nelkecompany.com</a></p>
                                       </span>

                                    </div><!--/. s-row-->
                                </div>
                        </article>

                    </div>
                    <div>
                        <article class="s-wrp adress-block">
                            <div class="address-item s-wrp">
                                <div class=" s-row"><!--s-row-->
                                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12 "><!-- s-col-->

                                        <span class="divide-address s-wrp">
                                         <p> H. Dhilaas Ground Floor</p>
                                        <p> Janavaree Magu</p>
                                        <p>Male 20047</p>
                                        <p>Republic of Maldives</p>
                                    </span>

                                        <span class="s-wrp s-lt-pad">
                                         <p>Tel:<a class="tel-num">+960 3004011, +960 3302702 </a></p>
                                         <p>Fax:<a class="tel-num">+960 3304860</a></p>
                                         <p>E-mail:<a class="tel-num">sales.marketing@nelkecompany.com</a></p>
                                       </span>

                                    </div><!--/. s-row-->
                                </div>
                        </article>
                    </div>
                    <div>
                        <article class="s-wrp adress-block">
                            <div class="address-item s-wrp">
                                <div class=" s-row"><!--s-row-->
                                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12 "><!-- s-col-->

                                        <span class="divide-address s-wrp">
                                         <p>H. Hanithila</p>
                                        <p>Buruzu Magu</p>
                                        <p>Male 20047</p>
                                        <p>Republic of Maldives</p>
                                    </span>

                                        <span class="s-wrp s-lt-pad">
                                         <p>Tel:<a class="tel-num">+960 3004011, +960 3335504</a></p>
                                         <p>Fax:<a class="tel-num">+960 3304860</a></p>
                                         <p>E-mail:<a class="tel-num">sales.marketing@nelkecompany.com</a></p>
                                       </span>

                                    </div><!--/. s-row-->
                                </div>
                        </article>
                    </div> <div>
                        <article class="s-wrp adress-block">
                            <div class="address-item s-wrp">
                                <div class=" s-row"><!--s-row-->
                                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12 "><!-- s-col-->

                                        <span class="divide-address s-wrp">
                                         <p>Thaa Atoll Shop</p>
                                        <p>Baoduthakurufaanu Magu</p>
                                        <p>Male 20047</p>
                                        <p>Republic of Maldives</p>
                                    </span>

                                        <span class="s-wrp s-lt-pad">
                                         <p>Tel:<a class="tel-num">+960 3004011, +960 3335504</a></p>
                                         <p>Fax:<a class="tel-num">+960 3304860</a></p>
                                         <p>E-mail:<a class="tel-num">sales.marketing@nelkecompany.com</a></p>
                                       </span>

                                    </div><!--/. s-row-->
                                </div>
                        </article>
                    </div>
                </div>
            </div>




            <div class="s-wrp contact-box s-hi-pad ">
                <div class=" s-row"><!--s-row-->
                    <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                        <div id="map">


                        </div>

                    </div><!--/. s-col-->


                    <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->
                        <article class="s-wrp adress-block">
                            <div class="s-wrp form-box">
                                <form class="contact-form" id="form">
                                    <h4 class=" nira-blue">Contact Form</h4>
                                    <p id="returnmessage"></p>
                                    <div class="s-form-group">
                                        <input type="text" id="name"  placeholder="Name">
                                    </div>
                                    <div class="s-form-group">
                                        <input type="text" placeholder="E-mail" id="email" class="email">
                                    </div>
                                    <div class="s-form-group">
                                        <input type="text" id="phone" placeholder="Phone" class="phone">
                                    </div>
                                    <div class="s-form-group">
                                        <textarea class="message-txt" id="message" placeholder="Message"></textarea>
                                    </div>
                                    <div class="s-form-group">
                                        <button type="button"  id="submit" value="Send Message" class="pg-btn" >SEND</button>
                                    </div>

                                </form>


                            </div>


                        </article>

                    </div><!--/. s-col-->



                </div><!--/. s-row-->
            </div>



        </article><!--/. s-wrp-->
    </div><!--/. container-->
</section><!--/. fourth_section-->





