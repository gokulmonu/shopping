<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script>

    var currentPage = 1;
    $body = $("body");

   /* $(document).on({
        ajaxStart: function() { $body.addClass("loading");    },
        ajaxStop: function() { $body.removeClass("loading"); }
    });*/
    $(function () {
        getTree();
        $('#auto_suggest').autocomplete({
            serviceUrl: '<?php echo base_url()?>product/auto-suggest/search',
            tabDisabled : true,
            onSelect: function (suggestion) {
                $('#phrase').val(suggestion.value);
                search(1);
            }
        });
        $('#auto_suggest').on('keyup', function (event) {
            event.preventDefault();
            var phrase = $('#auto_suggest').val();
            if(phrase.trim() == ''){
                search(1);
            }
        });
        $('#sort_by').on('change', function (event) {
            event.preventDefault();
                search(1);
        });

        $('#reset_tree').on('click', function (event) {
            event.preventDefault();
            $('#category_id').val('');
                getTree();
                search(1);
        });
    });
    function search (pagenum) {
        $body.addClass("loading");
        var phrase = $('#phrase').val();
        var sortOptions = $('#sort_by').val();
        var categoryId = $('#category_id').val();
        var autoSuggest = $('#auto_suggest').val();
        if(phrase != autoSuggest){
            phrase = autoSuggest;
        }
        $.ajax({
            url: '<?php echo base_url()?>product/search',
            type: 'POST',
            dataType: "html",
            data:{
                'phrase':phrase,
                'page':pagenum,
                'sort_by':sortOptions,
                'category_id' :categoryId

            },
            success: function (data) {
                $body.removeClass("loading");
                $('#product_container').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
    function openPage(pagenum) {
        //currentPage =pagenum;
        search(pagenum);
    }
    function getTree() {

        $.ajax({
            url: '<?php echo base_url()?>category/getTree',
            type: 'POST',
            success: function (data) {
                $('#tree').treeview({
                    data: data,
                    levels:0,
                    onNodeSelected:function (event, data) {
                        $('#category_id').val(data.href);
                        search(1);
                    }

                });
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
</script>
<body>
	<input type="hidden" id="category_id" value="">
     <div class="s-pgwrp"><!--s-pgwrp-->
        
        <section class="slider-wrp s-wrp"><!--slider-wrp-->
        	
            <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                    
                    <div class="slides" data-group="slides">
                        <ul>
                           
                            <li>
                                  <div class="slide-body" data-group="slide">
                                    
                                    <img src="<?php echo base_url() ?>bootstrap/images/slide-1.jpg" alt="Slider-Image1">
                                    
                                    
                                    <div class="caption header" data-animate="slideAppearRightToLeft" data-delay="800" data-length="300">
                                      


                                     
                                    
                                    </div>
                                    
                                  </div>
                            </li>
                            
                            
                            <li>
                                  <div class="slide-body" data-group="slide">
                                    
                                    <img src="<?php echo base_url() ?>bootstrap/images/slide-2.jpg" alt="Slider-Image2">
                                    
                                   <div class="caption header" data-animate="slideAppearRightToLeft" data-delay="800" data-length="300">
                                      


                                     
                                    </div>
                                    
                                    
                                  </div>
                            </li>
                            
                            <li>
                                  <div class="slide-body" data-group="slide">
                                    
                                    <img src="<?php echo base_url() ?>bootstrap/images/slide-4.jpg" alt="Slider-Image3">
                                    
                                    <div class="caption header" data-animate="slideAppearRightToLeft" data-delay="800" data-length="300">
                                      
                                     

                                  
                                    </div>
                                    
                                  </div>
                            </li>
                            
                           
                       </ul>
                    </div>
                    

                    <div class="pages">
                      <a class="page" href="#" data-jump-to="1">1</a>
                      <a class="page" href="#" data-jump-to="2">2</a>
                      <a class="page" href="#" data-jump-to="3">3</a>
                    </div>
                    
                    </div>

            	     
        </section><!--/. slider-wrp-->



     <section class="s-wrp s-hi-pad brdr-tp"><!-- section wrp-->
     <div class="s-container"><!-- s-container-->

     <div class="s-wrp"><!--s-wrp-->

     <div class="s-row"><!--f row-->

     <div class="s-col-lg-3 s-col-md-3 s-col-sm-4 s-col-xs-12"><!--f col-->

     <article class="s-wrp side-bar-sec  animated fadeInLeft"><!--side-bar-sec-->

     <div class="s-wrp each-block"><!--each-block-->


         <h3 class="side-bar-title s-sec-block">Categories</h3>
         <ul class="list-group">
         <li class="list-group-item node-tree" style="color:#FFFFFF;background-color:#0caad1;cursor: pointer;text-align: center" id="reset_tree">
             RESET
         </li></ul>
         <div id="tree"><!--tree-->

         </div><!--tree-->



     </div><!--/. each-block-->


         <div class="s-wrp each-block"><!--each-block-->


             <h3 class="side-bar-title s-sec-block">latest</h3>

             <article class="s-wrp each-box"><!--each-box-->

                 <div class="s-wrp s-in"><!--s-in-->

                     <ul class="s-wrp sellers-list">



                        <?php
                        if(isset($latest_products) && count($latest_products)){
                            foreach($latest_products as $latest){
                                $thumbimg = !empty($latest['thumbnail'])?$latest['thumbnail']:$this->config->item('default_thumb_image_url');
                                ?>
                                <li>

                                    <div class="s-row"><!--f row-->

                                        <div class="s-col-lg-4 s-col-md-4 s-col-sm-4 s-col-xs-12"><!--f col-->

                                            <article class="s-wrp">

                                                <a href="#" class="pic-box"><img alt="prod-3" src="<?php echo $thumbimg; ?>"></a>

                                            </article>


                                        </div><!--/. f col-->

                                        <div class="s-col-lg-8 s-col-md-8 s-col-sm-8 s-col-xs-12"><!--f col-->

                                            <article class="s-wrp">

                                                <ul class="detail-list">
                                                    <li>
                                                        <a href="<?php echo base_url() ?>product/view/<?php echo $latest['id'] ?>"><?php echo $latest['name']; ?></a>
                                                    </li>
                                                    <li>
                                                        <a class="" href="<?php echo base_url() ?>product/view/<?php echo $latest['id'] ?>">
                                                            View
                                                        </a>
                                                    </li>

                                                </ul>


                                            </article>

                                        </div><!--/. f col-->

                                    </div><!--/. f row-->


                                </li>



                            <?php }
                        }

                        ?>




                     </ul>



                 </div><!--/. s-in-->




             </article><!--/. each-box-->



         </div><!--/. each-block-->




     </article><!--/. side-bar-sec-->


     </div><!--/. f col-->



     <div class="s-col-lg-9 s-col-md-9 s-col-sm-8 s-col-xs-12"><!--f col-->

     <article class="s-wrp s-hi-pad product-list-block">
<form id="product_form">
     <div class="s-wrp  product-main-box"><!--product-main-box-->


         <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--f col-->

             <div class="s-wrp search-box">
                 <input type="text" placeholder="Search" id="auto_suggest"/>
                 <input type="hidden" id="phrase" value=""/>
                 <div id="search_but"  onclick="search(1)"> </div>
             </div>



         </div><!--/. f col-->



         <article class="sort-right-sec">

                       <span>Sort By:
                           <select class="sort" id="sort_by">
                               <option value="default">Default</option>
                               <option value="recently_added">Recently Added</option>
                               <option value="a_z">Alphabetical (A-Z)</option>
                               <option value="z_a">Alphabetical (Z-A)</option>
                           </select>
                       </span>

         </article>



     </div><!--/. product-main-box-->

</form>
     <div class="s-wrp s-hi-pad prod-list-sec" ><!-- prod-list-sec-->

     <div class="s-row" id="product_container"><!--f row-->

      <?php  $this->view('product/search_result',$data) ?>

     </div><!--/. f row-->

     </div><!--/. prod-list-sec-->



     </article>



     </div><!--/. f col-->



     </div><!--/. f row-->

     </div><!--/. s-wrp-->
     </div> <!--/. s-container-->
     </section>  <!--/. section wrp-->



  <section class="s-wrp s-hi-pad banner-sec brdr-tp"><!-- section wrp-->
     <div class="s-container"><!-- s-container-->

     <div class="s-wrp"><!--s-wrp-->

     <div class="s-row"><!--f row-->

     <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--f col-->


           <div class="s-wrp">
           
           <h1 class="s-txt-center white-txt">Essential Tool For Every Job</h1>
           <p class="s-txt-center">“We strive to grow and gain profit in our business by offering the best value for money to our customers. We seek to maintain a committed and highly motivated team dedicated to the achievement of excellence in service and conduct our business with honesty.”</p>
           </div>



     </div><!--/. f col-->



     </div><!--/. f row-->

     </div><!--/. s-wrp-->
     </div> <!--/. s-container-->
     </section>  <!--/. section wrp-->



         <script src="<?php echo base_url() ?>bootstrap/js/responsive-slider/jquery.event.move.js"></script>
         <script src="<?php echo base_url() ?>bootstrap/js/responsive-slider/responsive-slider.js"></script>


            
   </div><!--/. s-pgwrp-->



     <div class="modal"></div>
</body>
</html>