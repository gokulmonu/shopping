<script>

    function validateSignup () {
        var name = $('#name').val();
        var mobile = $('#mobile').val();
        var email = $('#email').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

// Encode the String
        var encodedString = Base64.encode(password);
        $.ajax({
            url: '<?php echo base_url()?>user/validate',
            type: 'POST',
            data:{
                'name' :name,
                'mobile' :mobile,
                'email' :email,
                'username':username,
                'password':encodedString

            },
            success: function (data) {
                $('#name_error').html('');
                $('#mobile_error').html('');
                $('#email_error').html('');
                $('#username_error').html('');
                $('#password_error').html('');
                if(!data.success){
                    for(key in data){
                        $('#'+data[key]['field_id']).html(data[key]['label']);
                    }
                }
                if(typeof  data.url != "undefined"){
                    if(data.url.success){
                        alert("successfully you are registered with us");
                        window.location.href ='<?php echo base_url() ?>login';
                    }
                }


            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
</script>
<head>
    <meta charset="uts-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Hardware-Product</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="32x32" href="fav.png">

    <!--stylesheets-->

    <link href="https://fonts.googleapis.com/css?family=BioRhyme" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Chicle" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/animate.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/base.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/s-controls.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/custom.css"/>


    <!--/. stylesheets-->

    <script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/jquery-2_1_4.js"></script>

    <!--/. js files-->


</head>


<body>


<div class="s-pgwrp login-body"><!--s-pgwrp-->

    <section class="login-wrp s-wrp"><!--login wrp-->

        <div class="s-container"><!--s-container-->

            <div class="s-row"><!--f row-->
                <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!--f col-->

                    <article class="s-wrp welcome-sec signup-resp">

                        <h3 class="white-txt welcome-txt">Welcome to </h3>
                        <h1 class="title-txt">Hardware Lab</h1>

                        <p class="white-txt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                            Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                            ridiculus mus. </p>

                    </article>



                </div><!--/. f col-->



                <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!--f col-->



                    <section class="login-sec s-wrp"><!--login-sec-->

                        <div class="s-in s-wrp">

                            <h4 class="white-txt login-head s-txt-center">Create Your Account</h4>
                            <div class="s-row"><!--s-row-->


                                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                                    <form class="s-in">
                                        <div class="s-row"><!--s-row-->
                                            <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->
                                                <div class="s-form-group">
                                                    <label>Name</label>
                                                    <input type="text" id="name" class="s-form-control" placeholder="Enter your name">
                                                    <span class="error-msg" id="name_error"></span>
                                                </div>
                                            </div><!--/. s-col-->
                                            <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->
                                                <div class="s-form-group">
                                                    <label>Mobile</label>
                                                    <input type="text" id="mobile" class="s-form-control" placeholder="Enter your mobile">
                                                    <span class="error-msg" id="mobile_error"></span>
                                                </div>
                                            </div><!--/. s-col-->
                                        </div><!--/. s-row-->

                                        <div class="s-row"><!--s-row-->
                                            <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->
                                                <div class="s-form-group">
                                                    <label>Email</label>
                                                    <input type="email" id="email" class="s-form-control" placeholder="Enter your email">
                                                    <span class="error-msg" id="email_error"></span>
                                                </div>
                                            </div><!--/. s-col-->
                                            <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->
                                                <div class="s-form-group">
                                                    <label>Username</label>
                                                    <input type="text" id="username" class="s-form-control" placeholder="Enter your Username">
                                                    <span class="error-msg" id="username_error"></span>
                                                </div>
                                            </div><!--/. s-col-->
                                        </div><!--/. s-row-->

                                        <div class="s-row"><!--s-row-->
                                            <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                                                <div class="s-form-group">
                                                    <label>Password</label>
                                                    <input type="password" id="password" class="s-form-control" placeholder="Enter your Password">
                                                    <span class="error-msg" id="password_error"></span>
                                                </div>
                                            </div><!--/. s-col-->
                                            <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->
                                                <div class="s-form-group">
                                                    <label>Confirm Password</label>
                                                    <input type="password" id="password" class="s-form-control" placeholder="Enter your Password">
                                                    <span class="error-msg" id="password_error"></span>
                                                </div>
                                            </div><!--/. s-col-->
                                        </div><!--/. s-row-->


                                        <div class="s-row"><!--s-row-->
                                            <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                                                <div class="s-form-group">
                                                    <input type="button" class="login-btn hvr-wobble-top " value="Sign Up" onclick="validateSignup()">
                                                    <a class="forget-link" href="<?php echo base_url().'login'?>">Already Member?</a>
                                                </div>
                                            </div><!--/. s-col-->
                                        </div><!--/. s-row-->

                                    </form>

                                </div><!--/. s-col-->

                            </div><!--/. s-row-->


                        </div>




                    </section><!--/. login-sec-->





                </div><!--/. f col-->


            </div><!--/. f row-->

        </div><!--/. s-container-->

    </section><!--/. login-wrp -->







</div>
<!--/. s-pgwrp-->


</body>
