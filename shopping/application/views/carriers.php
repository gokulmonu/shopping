
<?php
$admin = false;
$userGroup= $this->core_lib->getUserGroup();
if(in_array("admin",$userGroup)){
    $admin  = true;
}
?>
<section class="s-wrp"><!--second_section-->

    <div class="s-wrp title-strip">
        <h2 class="s-txt-center white-txt">Careers</h2>
    </div>

    <div class="s-container"><!--container-->
        <?php
        if($admin){ ?>

        <span class="btn-wrp"> <a class="pg-btn" href="<?php echo base_url()?>career/add/">Add New</a></span>
        <?php }

        ?>
        <?php
        if(isset($rows) && count($rows)){
            foreach($rows as $values){
            ?>

            <article class="s-wrp s-md-pad">

                <article class="s-wrp solution-extra-area">

                    <h4 class="about-title"><?php echo $values['title'] ?></h4>
                    <p><span class="ui-red">Salary :</span><?php echo $values['salary'] ?></p>
                    <p><span class="ui-red">Experiance :</span><?php echo $values['experiance'] ?></p>
                    <p>
                        <span class="ui-red">Description :</span><?php echo $values['description'] ?>
                    </p>
                </article>
                <?php
                if($admin){ ?>

                    <span class="btn-wrp"> <a class="pg-btn" href="<?php echo base_url()?>career/edit/<?php echo $values['id']?>">Edit</a></span>
                    <span class="btn-wrp"> <a class="pg-btn" href="<?php echo base_url()?>career/delete/<?php echo $values['id']?>">Delete</a></span>

                <?php }

                ?>

            </article>
        <?php }
        }else{ ?>
        <article class="s-wrp s-md-pad">
            <h4 class="ui-red">No careers found</h4>
        </article>
        <?php

        }

        ?>






    </div><!--/. container-->
</section><!--/. second_section-->