<!--stylesheets-->

<!--<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=BioRhyme" rel="stylesheet">-->
<link href="https://fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Arial+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/font-awesome.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/animate.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/js/owl-carousel/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/js/owl-carousel/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/js/owl-carousel/owl.transitions.css" />

<link rel="stylesheet" type="text/css" href='<?php echo base_url() ?>bootstrap/js/responsive-slider/responsive-slider.css' />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/js/fancy-box/jquery.fancybox.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/js/tool-tip/tooltip.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/js/date-picker/datepicker.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/base.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/s-controls.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/nav-responsive.css" />

<link href="<?php echo base_url() ?>bootstrap/js/accordion/accordion.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/inner-custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/bootstrap-treeview.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/js/responsive-tabs/easy-responsive-tabs.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/dropzone.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>bootstrap/css/iziModal.min.css"/>
<!--/. stylesheets-->


<!--js files-->
<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/jquery-2_1_4.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/dropzone/dropzone.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/izimodal/iziModal.min.js"></script>


<script  type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/responsive-tabs/easyResponsiveTabs.js"></script>
<script  type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/responsive-tabs/tabs-script.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/fancy-box/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/fancy-box/fancy-box-script.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/tool-tip/tooltipsy.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/tool-tip/tooltipsy-script.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/accordion/accordion.js"></script>

<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/owl-carousel/owl-script.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/owl-carousel/owl-thumbnail.js"></script>


<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/nav-responsive.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>bootstrap/js/scroll-top.js"></script>

<!--<script type="text/javascript" src="--><?php //echo base_url() ?><!--bootstrap/js/jquery.sticky.js"></script>-->


<script src="<?php echo base_url() ?>bootstrap/js/auto-complete/jquery.autocomplete.js"></script>
<script src="<?php echo base_url() ?>bootstrap/js/tree-view/bootstrap-treeview.js"></script>

<!-- <script type="text/javascript" src="--><?php //echo base_url() ?><!--bootstrap/js/custom.js"></script>-->

<!--/. js files-->

<head>
    <meta charset="uts-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Hardware Lab Home</title>
    <meta name="description" content="Hardware Lab male NELKE Pvt. Ltd., is a registered company at the Ministry of Economic Development, (Registry Number:C – 98 / 2004) to provide reliable and efficient services in the areas of, Import/Export business, Building Construction and Travel & Tourism Business.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="32x32" href="fav.png">
</head>
<div class="s-pgwrp"><!--s-pgwrp-->

    <header class="header-wrp s-wrp"><!--header-wrp-->

        <section  class="logo-wrp s-wrp "><!--logo-wrp-->

            <div class="s-container"><!-- s-container-->


                    <div class="s-row"><!--f row-->

                    <div class="s-col-lg-3 s-col-md-3 s-col-sm-12 s-col-xs-12"><!--f col-->

                     <a class="logo-a" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>bootstrap/images/logo2.png"></a>

                    </div><!--/. f col-->


                    <div class="s-col-lg-9 s-col-md-9 s-col-sm-12 s-col-xs-12"><!--f col-->

                        <ul class="custom-nav"  data-type="navbar">
                            <li> <a href="<?php echo base_url()?>about-us">About Us</a> </li>
                            <li><a href="<?php echo base_url()?>services">Services</a></li>
                            <li><a href="<?php echo base_url()?>careers">Careers</a></li>
                            <li> <a href="<?php echo base_url()?>contact-us">Contact Us</a> </li>
                             <li class="cart-icon"> <a href="<?php echo base_url()?>cart/view"><i class="fa fa-shopping-cart"></i> </a> <span class="cart-counter"><?php echo $this->core_lib->getCartCount() ?></span></li>


                            <?php

                            $isLoggedIn = $this->core_lib->getLoginUserId();
                            $admin = false;
                            $userGroup= $this->core_lib->getUserGroup();
                            if(in_array("admin",$userGroup)){
                                $admin  = true;
                            }
                            if($admin){?>

                                <li class="custom-dropdown">
                                    <a href="#" class="account">Admin</a>

                                    <ul class="sub_list">
                                        <li><a  href="<?php echo base_url()?>category" class="profile ul-category">Categories</a></li>
                                        <li><a href="<?php echo base_url()?>product/add/" class="profile ul-product">Add Product</a></li>
                                        <li><a href="<?php echo base_url()?>logout" class="signout ul-signout">Signout</a></li>

                                </li>
                        </ul>
                                <?php

                            }elseif($isLoggedIn){?>
                            <li class="custom-dropdown">
                                <a href="#" class="account">Hi ,Welcome</a>

                                <ul class="sub_list">
                                    <li><a href="<?php echo base_url()?>logout" class="signout ul-signout">Signout</a></li>

                                    </li>
                            <?php
                            } else{
                                ?>
                                <li>
                                    <a href="<?php echo base_url() ?>login" class="account">Login</a>
                                </li>

                                <?php
                            }
                            ?>
                                   
                        </ul>


                    </div><!--/. f col-->


                </div><!--/. f row-->

            </div><!--/. s-container-->

        </section><!--/. logo-wrp -->


    </header><!--/. header-wrp-->
</div><!--s-pgwrp-->