
<div class="s-pgwrp"><!--s-pgwrp-->
<div class="s-wrp pagination-sec"><!--pagination-sec-->

         <div class="s-row"><!--f row-->

             <div class="s-col-lg-6 s-col-md-8 s-col-sm-12 s-col-xs-12"><!--f col-->


                 <ul class="pagination">

                     <?php
                     if(isset($pagination) && count($pagination)>1){
                         $loopCount = count($pagination);
                         if($pagination[0]['text']!= "First"){
                          ?>
                             <li>
                                 <span>First</span>
                             </li>
                             <?php
                         }
                         foreach ($pagination as $value){
                             ?>

                     <li <?php if($value['current_page']){?>class="pagin-active" <?php

                     }?>>
                             <a href="javascript:;" onclick="openPage('<?php echo $value['page_num'] ?>')"> <span><?php echo $value['text'] ?></span></a>
                     </li>
                         <?php
                         }
                         if($pagination[$loopCount-1]['text'] != 'Last'){
                             ?>
                             <li>
                                  <span>Last</span>
                             </li>
                             <?php

                         }
                     }
                     ?>


                 </ul>

             </div><!--/. f col-->
         </div><!--/. f row-->




     </div><!--/. pagination-sec-->
</div>
