
                    <article class="s-wrp list-block">


                        <div class="tbl-box f-wrp"><!--tbl box-->

                            <div class="scroll-div">

                                <table class="table-responsive normal-table custom-table">
                                    <thead>
                                    <tr>
                                        <th>NAME</th>
                                        <th>PARENT CATEGORY</th>
                                        <th>STATUS</th>
                                        <th>ACTION</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(isset($data['rows']) && count($data['rows'])){
                                        foreach ($data['rows'] as $value){
                                            ?>
                                            <tr>
                                                <td><?php echo $value['name'] ?></td>
                                                <td>Hinges</td>
                                                <td>Active</td>
                                                <td>
                                                    <a class="action-btn" href="<?php echo base_url()?>category/view/<?php echo $value['id'] ?>" title="view details"><i class="fa fa-eye"></i></a>
                                                    <a class="action-btn" href="<?php echo base_url()?>category/edit/<?php echo  $value['id'] ?>" title="edit details"><i class="fa fa-edit"></i></a>
                                                    <a class="action-btn" href="javascript:;" title="delete" onclick="deleteCategory('<?php echo $value['id'] ?>')"><i class="fa fa-trash"></i></a>
                                                </td>


                                            </tr>
                                    <?php
                                        }
                                    }else{
                                        ?>
                                        <td>No results Found</td>
                                    <?php
                                    }

                                    ?>

                                    </tbody>

                                </table>

                            </div>

                        </div>






                    </article>

<?php
if(isset($data['pagination'])){
    $dataArr = [
        'pagination'=>$data['pagination']
    ];
    $this->view('pagination',$dataArr);
}

?>
