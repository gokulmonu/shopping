<script>
    //var currentPage = 1;
    $(function () {
       $('#phrase').on('keyup',function () {
           setTimeout(search(1),1000);
        });

    });
    function search (curpage) {
        var phrase = $('#phrase').val();
        $.ajax({
            url: '<?php echo base_url()?>category/search',
            type: 'POST',
            dataType: "html",
            data:{
                'phrase':phrase,
                'page':curpage

            },
            success: function (data) {
                $('#category_result_container').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
    function deleteCategory(categoryId) {
        if(confirm("Are you sure want to delete this category and its subcategory?")){
            var postData = {};
            postData['category_id'] = categoryId;
            $.ajax({
                url: '<?php echo base_url()?>category/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                    location.reload();
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        }else{
            return false;
        }

    }
    function openPage(pagenum) {
        search(pagenum);
    }
</script>
<div class="s-pgwrp"><!--s-pgwrp-->
    <section class="s-wrp s-hi-pad"><!-- section wrp-->
        <div class="s-container"><!-- s-container-->

            <div class="s-wrp"><!--s-wrp-->

                <div class="s-row"><!--s row-->

                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                        <article class="s-wrp title-block">

                            <h2 class="main-title">Categories</h2>
                            <a class="add-btn" href="<?php echo base_url() ?>category/add/0"><i class="fa fa-plus"></i> Add Category</a>


                        </article>



                    </div>
                    <!--/. s col-->

                </div>
                <!--/. s row-->

            </div>
            <!--/. s-wrp-->
        </div>
        <!--/. s-container-->
    </section>
    <!--/. section wrp-->

    <section class="s-wrp s-lt-pad"><!-- section wrp-->
        <div class="s-container"><!-- s-container-->

            <div class="s-wrp"><!--s-wrp-->

                <div class="s-row"><!--s row-->

                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                        <article class="s-wrp search-block">

                            <h5 class="search-title">Search by Phrase</h5>
                            <input type="text" class="search-input" placeholder="Search by phrase" id="phrase">

                        </article>



                    </div>
                    <!--/. s col-->

                </div>
                <!--/. s row-->

            </div>
            <!--/. s-wrp-->
        </div>
        <!--/. s-container-->
    </section>
</div>