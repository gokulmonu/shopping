<script>
    var currentPage = 1;
    $(function () {
        $('#sub_phrase').on('keyup',function () {
            setTimeout(search,1000);
        });

    });
    function search () {
        var phrase = $('#sub_phrase').val();
        $.ajax({
            url: '<?php echo base_url()?>category/search',
            type: 'POST',
            dataType: "html",
            data:{
                'phrase':phrase,
                'parent_id' :'<?php echo  $details['id'] ?>',
                'page':currentPage

            },
            success: function (data) {
                $('#sub_category_container').html(data);
            },
            error: function (e) {
                //called when there is an error
                //console.log(e.message);
            }
        });
    }
    function deleteCategory(categoryId) {
        if(confirm("Are you sure want to delete this category and its subcategory?")){
            var postData = {};
            postData['category_id'] = categoryId;
            $.ajax({
                url: '<?php echo base_url()?>category/delete',
                type: 'POST',
                data: postData,
                success: function (data) {
                    window.location.href = '<?php  echo base_url()."category/"?>';
                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });
        }else{
            return false;
        }

    }
    function openPage(pagenum) {
        currentPage =pagenum;
        search();
    }
</script>
<div class="s-pgwrp"><!--s-pgwrp-->
    <section class="s-wrp s-hi-pad"><!-- section wrp-->
        <div class="s-container"><!-- s-container-->

            <div class="s-wrp"><!--s-wrp-->

                <div class="s-row"><!--s row-->

                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                        <article class="s-wrp edit-block">

                            <h2 class="main-title">View Category</h2>

                        </article>
                    </div>
                    <!--/. s col-->

                </div>
                <!--/. s row-->

            </div>
            <!--/. s-wrp-->
        </div>
        <!--/. s-container-->
    </section>
    <!--/. section wrp-->



    <section class="s-wrp"><!-- section wrp-->
        <div class="s-container"><!-- s-container-->

            <div class="s-wrp"><!--s-wrp-->

                <div class="s-row"><!--s row-->

                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                        <article class="s-wrp button-sec s-lt-pad">

                            <a class="page-btn" href="<?php echo base_url()?>category/edit/<?php echo  $details['id'] ?>"><i class="fa fa-edit"></i> Edit This Category</a>
                            <a class="page-btn" href="<?php echo base_url()?>category/add/<?php echo  $details['id'] ?>"><i class="fa fa-plus"></i> Add Sub Category</a>
                            <a class="page-btn" href="javascript:;" onclick="deleteCategory('<?php echo  $details['id'] ?>')"><i class="fa fa-trash"></i> Delete This Category</a>

                           <?php

                           if($details['parent_id']){
                               ?>
                               <a class="page-btn" href="<?php echo base_url()?>category/view/<?php echo  $details['parent_id'] ?>"><i class="fa fa-chevron-circle-left"></i> Back To Parent View</a>

                               <?php

                           }

                           ?>
                             <a class="page-btn" href="<?php echo base_url()?>category"><i class="fa fa-chevron-circle-left"></i> Back To Categories</a>

                        </article>



                    </div>
                    <!--/. s col-->

                </div>
                <!--/. s row-->

            </div>
            <!--/. s-wrp-->
        </div>
        <!--/. s-container-->
    </section>
    <!--/. section wrp-->

    <section class="s-wrp s-md-pad"><!-- section wrp-->
        <div class="s-container"><!-- s-container-->

            <div class="s-wrp"><!--s-wrp-->

                <div class="s-row"><!--s row-->

                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                        <div class="hor_tab custom-tab">

                                <ul class="resp-tabs-list hor_1 s-align-center">
                                    <li>DETAILS</li>
                                    <li>SUB-CATEGORIES</li>
                                </ul>


                                <div class="resp-tabs-container hor_1">



                                    <div class="each_tab_content s-wrp">

                                        <article class="view-item s-wrp">

                                            <div class="s-form-group">

                                                <div class="s-col-lg-2 s-col-md-2 s-col-sm-2 s-col-xs-3 "><!--s-col-->
                                                    <label>Name:</label>
                                                </div> <!--/. s col-->
                                                <div class="s-col-lg-10 s-col-md-10 s-col-sm-10 s-col-xs-9 "><!--s-col-->
                                                    <span><?php echo $details['name'] ?></span>
                                                </div> <!--/. s col-->
                                            </div>

                                            <div class="s-form-group">
                                                <div class="s-col-lg-2 s-col-md-2 s-col-sm-2 s-col-xs-3 "><!--s-col-->
                                                    <label>Description:</label>
                                                </div> <!--/. s col-->
                                                <div class="s-col-lg-10 s-col-md-10 s-col-sm-10 s-col-xs-9 "><!--s-col-->
                                                    <span><?php echo $details['description'] ?></span>
                                                </div> <!--/. s col-->
                                            </div>

                                            <div class="s-form-group">
                                                <div class="s-col-lg-2 s-col-md-2 s-col-sm-2 s-col-xs-3 "><!--s-col-->
                                                    <label>Status:</label>
                                                </div> <!--/. s col-->
                                                <div class="s-col-lg-10 s-col-md-10 s-col-sm-10 s-col-xs-9 "><!--s-col-->
                                                    <span>Active</span>
                                                </div> <!--/. s col-->
                                            </div>

                                        </article>



                                    </div>


                                    <div class="each_tab_content s-wrp">

                                        <section class="s-wrp s-lt-pad"><!-- section wrp-->


                                            <div class="s-wrp"><!--s-wrp-->

                                                <div class="s-row"><!--s row-->

                                                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                                                        <article class="s-wrp search-block">

                                                            <h5 class="search-title">Search by Phrase</h5>
                                                            <input type="text" id="sub_phrase" class="search-input" placeholder="Search by phrase">

                                                        </article>



                                                    </div>
                                                    <!--/. s col-->

                                                </div>
                                                <!--/. s row-->

                                            </div>
                                            <!--/. s-wrp-->

                                        </section>
                                        <!--/. section wrp-->
                                        <div id="sub_category_container">
                                            <?php
                                            $this->view('category/search_result',$data);
                                            ?>
                                        </div>


                                    </div>






                                </div>

                        </div>




                    </div>
                    <!--/. s col-->

                </div>
                <!--/. s row-->

            </div>
            <!--/. s-wrp-->
        </div>
        <!--/. s-container-->
    </section>
    <!--/. section wrp-->




</div>
<!--/. s-pgwrp-->
