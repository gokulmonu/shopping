<?php
$this->view('category/search_filter');
?>
<div id="category_result_container">

    <section class="s-wrp s-md-pad "><!-- section wrp-->
        <div class="s-container"><!-- s-container-->

            <div class="s-wrp"><!--s-wrp-->

                <div class="s-row"><!--s row-->

                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                        <?php
$this->view('category/search_result',$data);
?>


                    </div>
                    <!--/. s col-->

                </div>
                <!--/. s row-->

            </div>
            <!--/. s-wrp-->
        </div>
        <!--/. s-container-->
    </section>
    <!--/. section wrp-->
</div>
