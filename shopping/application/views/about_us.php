<section class="s-wrp s-md-pad brdr-tp"><!--second_section-->
    <div class="s-container"><!--container-->


        <div class=" s-row"><!--s-row-->
            <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!-- s-col-->


                <article class="s-wrp about-box">


                    <article class="s-wrp solution-extra-area">
                        <span class="abt-img animated wow fadeInLeft" data-wow-delay="15ms"><img src="<?php echo base_url() ?>bootstrap/images/nelke logo.jpg"></span>

                        <h4>Introduction</h4>

                   <p>NELKE’Pvt. Ltd., is a registered company at the Ministry of Economic Development, (Registry Number:C – 98 / 2004) to provide reliable and efficient services in the areas of, Import/Export business, Building Construction and Travel & Tourism Business.</p>
                    </article>

                    <article class="s-wrp solution-extra-area">

                        <h4 class="about-title">The Board of Directors</h4>
                        <p>The Board of Directors includes a team of young, energetic and experienced members that belongs to same familyand arehighly skilled on Building and Development, Wholesale/Retail and Hospitality/Travel Management.</p>
                        <p>1.Mr. Shahul Hameed	(Managing Director)</p>
                        <p>2.Mr. Moosa Anwar	(Director)</p>
                        <p>3.Mr. HasaanZahid	(Director)</p>
                        <p>4.Mr. Abdulla Shariu	(Director)</p>
                    </article>

                    <article class="s-wrp solution-extra-area">

                      <p>
                          The company started with a single outlet that retailed garments in Majeedhee Magu. The philosophy was sustainable growth and after the first year where the hard work of the founders paid off with increased growth, a second outlet was opened. After a few years and more growth, the team at Nelke’ Company Pvt. Ltd decided to diversify and a 3rd outlet was opened targeting household goods market. By 2012 the company had 6 outlets in Male.

                      </p>

                        <p>
                            In early 2012 a decision was made by the directors, for the company to venture into the market of retailing general hardware. The construction industry is the 5th biggest GDP contributor to the Maldivian economy and as such the demand for general hardware and construction materials was high and this was the reasoning behind the decision to diversify. What followed was a full restructure of the existing businesses and liquidating existing capital. By the end of 2012 after some substantial investment the company jumped into the new sector with the business name of ‘Hardware Lab’.


                        </p>

                        <p>Over the last 3 years the company has grown significantly with business growth of over 50% each year. Hardware Lab has slowly become household name within the market. </p>

                        <p>We have 2 outlets for retailing, a dedicated team for corporate clients, delivery service of the goods and 7 warehouse. Each of our 30 staff are trained and taught to always deal with our clients with respect and honesty.

                        </p>

                    </article>
                    <article class="s-wrp solution-extra-area">

                        <h4 class="about-title">Mission Statement</h4>
                      <p>“We strive to grow and gain profit in our business by offering the best value for money to our customers.

                          We seek to maintain a committed and highly motivated team dedicated to the achievement of excellence in service and conduct our business with honesty.”
                      </p>
                    </article>

                     <article class="s-wrp solution-extra-area">

                        <h4 class="about-title">Objectives</h4>
                     <p>We seek to conduct our businesses responsibly and achieve goals while remaining competitive and financially strong. We seek to expand and grow our business sustainably and venture into new markets. </p>
                    </article>

                     <article class="s-wrp solution-extra-area">

                        <h4 class="about-title">Our Principles and Values</h4>

                         <p>We believe that our employees are a valuable asset, and provide them security and opportunities. We respect human rights and provide safe and good working conditions and equal employment opportunities for our employees.

                         </p>

                         <p>We believe that our customers are essential to our success.  We try to provide them with products and services that give them value that is worth the price they pay. </p>
                          <p>We believe that our relations with our suppliers, contractors and partners are very important. We believe that honesty and respect is very important in such relationships.</p>

                      </article>

                </article>

                <article class="s-wrp s-md-pad">
                    <h4 class="about-title">Properties</h4>


                    <div class=" s-row"><!--s-row-->
                        <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                            <article class="s-wrp each-service">

                                <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/wholesale.jpg"> </span>

                                <h5 class="s-txt-up">Hardware Lab, Janavaree Magu</h5>

                              <p>This is Nelke’ Company’s flagship hardware outlet and also where the company office is located. Many of the big corporate supply contracts are established from here. </p>
                            </article>


                        </div><!--/. s-col-->
                        <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                            <article class="s-wrp each-service">

                                <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/lab2.jpg"> </span>

                                <h5 class="s-txt-up">Hardware Lab, Buruzu Magu</h5>
<p>Second Hardware LAB store, in Buruzu Magu, a central street of Male which is the capital of the Maldives.  By branching out to another area enables the company to cater for a bigger customer base.</p>
                            </article>


                        </div><!--/. s-col-->

                        <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                            <article class="s-wrp each-service">

                                <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/img3.jpg"> </span>

                                <h5 class="s-txt-up">Hardware Lab, Bazar Shop</h5>
<p>Our third Hardware LAB store, in Thaa Atoll Shop BaoduthakurufaanuMagu.  By branching out to another area enables the company to cater for a bigger customer base.</p>
                            </article>


                        </div><!--/. s-col-->


                    </div><!--/. s-row-->

                </article>

            </div><!--/. s-col-->

        </div><!--/. s-row-->


    </div><!--/. container-->
</section><!--/. second_section-->