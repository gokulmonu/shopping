<section class="s-wrp"><!--second_section-->

    <div class="s-wrp title-strip">
        <h2 class="s-txt-center white-txt">Our Stores</h2>
    </div>

    <div class="s-container"><!--container-->


        <article class="s-wrp s-md-pad">


            <div class=" s-row"><!--s-row-->
                <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                    <article class="s-wrp each-service">

                        <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/wholesale.jpg"> </span>

                        <h5 class="s-txt-up">Hardware Lab, Janavaree Magu</h5>

                        <p>
                            This is Nelke’ Company’s flagship hardware outlet and also where the company office is located. Many of the big corporate supply contracts are established from here.


                        </p>

                    </article>


                </div><!--/. s-col-->
                <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                    <article class="s-wrp each-service">

                        <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/property2.jpg"> </span>

                        <h5 class="s-txt-up">Hardware Lab, Buruzu Magu</h5>

                        <p>
                            Second Hardware Lab store, in Buruzu Magu, a central street of Male which is the capital of the Maldives.  By branching out to another area enables the company to cater for a bigger customer base.

                        </p>

                    </article>


                </div><!--/. s-col-->

                <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                    <article class="s-wrp each-service">

                        <span class="service-img"><img src="<?php echo base_url()?>bootstrap/images/img3.jpg"> </span>

                        <h5 class="s-txt-up">Hardware Lab, Bazar Shop</h5>
                        <p>Our third Hardware LAB store, in Thaa Atoll Shop BaoduthakurufaanuMagu.  By branching out to another area enables the company to cater for a bigger customer base.</p>
                    </article>


                </div><!--/. s-col-->


            </div><!--/. s-row-->
        </article>

        <article class="s-wrp solution-extra-area pad s-md-pad">

            <h4 class="about-title">Future Development</h4>
            <p>

                We also have ambitions to diversify into the food wholesale sector under the name of ‘Nelke Foods’. Our strong contacts in countries like UAE, Thailand and Indonesia will enable us to sell goods at competitive prices. Under this business we also have plans to open restaurants.

            </p>

            <p>

                The next business venture for Nelke Company is to move forward in the construction sector. With that in mind the Directors have come to an agreement to start its new construction wing under the name ‘Construction Lab’


            </p>

        </article>




    </div><!--/. container-->
</section><!--/. second_section-->